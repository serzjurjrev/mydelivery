<?php

require_once('./class/PHPExcel.php');
require_once('./class/PHPExcel/Writer/Excel5.php');


class createTables
{
    const COMPANY_NAME = 'My Delivery OÜ';
    const ADDRESS      = 'Pärnu mnt 48a-5, Tallinn, 10119';
    const REG          = 'Reg. nr: 14005202';
    const SITE         = 'www.mydelivery.ee';
    const EMAIL        = 'info@mydelivery.ee';
    const PHONE        = 'Tel: +372 5682 2485';
    const BANK_NAME    = 'Swedbank';
    const BANK_ACCOUNT = 'EE652200221063736611';
    const BANK_SWIFT   = 'SWIFT: HABAEE2X';

    const FONT_SIZE = 10;
    const ROW_HEIGHT = 12.75;
    const RATIO = 0.71; //this ratio to set right column width (column width + ratio)

    //styles
    const BORDERS = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            )
        ),
    );

    const FILL_COLOR = array(
            'fill' =>array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color'   => array(
                    'rgb' => 'D7D7D7'
                )
            ),
            'font' => array(
                'bold' => true,
            ),
        );

    const NUMBER_FORMAT = array(
        'code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00
    );

    private $xls;
    private $sheet;
    private $logo;


    public function __construct(PHPExcel $xls, PHPExcel_Worksheet_Drawing $logo)
    {
        $this->xls = $xls;
        $this->xls->setActiveSheetIndex(0);
        $this->sheet = $this->xls->getActiveSheet();
        $this->logo = $logo;


    }

    public function createWorkSheetTitle()
    {
        $this->sheet->setTitle('aruanne');
    }

    public function setDimenitions( array $columnsWidth )
    {
        $this->sheet->getDefaultRowDimension()->setRowHeight(self::ROW_HEIGHT);
        $this->sheet->getParent()->getDefaultStyle()->getFont()->setSize(self::FONT_SIZE);

        foreach($columnsWidth as $column)
        {
            $this->sheet->getColumnDimension($column['column'])->setWidth($column['width'] + self::RATIO);
        }

    }

    public function drawPaymentsTables( $paymentsArray )
    {
        $this->headTitle();

        if( is_array($paymentsArray) ){

            //set customer
            if( isset($paymentsArray['restaurant']) )
            {
                $this->setCustomer($paymentsArray['restaurant']);
            }

            //set customer reg nr
            if( isset($paymentsArray['restaurant_reg']) )
            {
                $this->setCustomerReg($paymentsArray['restaurant_reg']);
            }

            //set customer address
            if( isset($paymentsArray['restaurant_address']) )
            {
                $this->setCustomerAddress($paymentsArray['restaurant_address']);
            }

            //set customer email
            if( isset($paymentsArray['restaurant_email']) )
            {
                $this->setCustomerEmail($paymentsArray['restaurant_email']);
            }

            //set customer phone
            if( isset($paymentsArray['restaurant_phone']) )
            {
                $this->setCustomerPhone($paymentsArray['restaurant_phone']);
            }

            //set report id
            if( isset($paymentsArray['report_id']) )
            {
                $this->setReportId($paymentsArray['report_id']);
            }

            //set date of creation
            if( isset($paymentsArray['report_created']) )
            {
                $this->setReportCreated($paymentsArray['report_created']);
            }

            //set period
            if( isset($paymentsArray['report_period_from']) && isset($paymentsArray['report_period_to']) )
            {
                $this->setPeriod($paymentsArray['report_period_from'], $paymentsArray['report_period_to']);
            }

            //set main table
            $rowNr = $this->setMainTable($paymentsArray);


            //set payments online table
            if(isset($paymentsArray['report_order_online']))
            {
                $rowNr++;
                $rowNr = $this->setPaymentsTable($paymentsArray['report_order_online'], $rowNr, 'Tasumine online');
            }

            //set payments cash table
            if(isset($paymentsArray['report_order_cash']))
            {
                $rowNr = $rowNr + 3;
                $rowNr = $this->setPaymentsTable($paymentsArray['report_order_cash'], $rowNr, 'Tasumine sularahas toitlustajale');
            }

            $rowNr++;
            $rowNr = $this->drawLine($rowNr);

            $this->setCompanyDetails($rowNr);
        }
    }

    /**
     * set title 'ARVESTUSPERIOODI ARUANNE'
     *
     */
    public function headTitle()
    {
        //styles for c12
        $styleArray = array(
            'font' => array(
                'bold' => true,
                'size' => 12
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );

        $this->sheet->setCellValue("C12", 'ARVESTUSPERIOODI ARUANNE');
        $this->sheet->getRowDimension(12)->setRowHeight(15.75);
        $this->sheet->getStyle("C12")->applyFromArray($styleArray);
    }

    /**
     * @param $customer string
     */
    public function setCustomer( $customer )
    {
        //todo: check if an array
        $this->sheet->setCellValue('A4', 'Toitlustaja:');
        $this->sheet->setCellValue('A5', $customer);
    }

    /**
     * @param $customerReg string
     */
    public function setCustomerReg( $customerReg )
    {
        $this->sheet->setCellValue('A6', $customerReg);
    }

    /**
     * @param $customerAddr string
     */
    public function setCustomerAddress( $customerAddr )
    {
        $this->sheet->setCellValue('A7', $customerAddr);
    }

    /**
     * @param $customerEmail string
     */
    public function setCustomerEmail( $customerEmail )
    {
        $this->sheet->setCellValue('A8', $customerEmail);
    }

    /**
     * @param $customerPhone string
     */
    public function setCustomerPhone( $customerPhone )
    {
        $this->sheet->setCellValue('A9', 'Tel. '.$customerPhone);
    }

    /**
     * @param $created string
     */
    public function setReportCreated( $created )
    {
            $this->sheet->setCellValue("E14", 'Kuupäev:');
            $this->sheet->setCellValue("F14", $created);
    }

    /**
     * @param $id string
     */
    public function setReportId( $id )
    {

            $this->sheet->setCellValue("E13", 'Aruanne nr:    '.stristr($id, '-', true).'-');
            $this->sheet->setCellValue("F13", substr($id, strpos($id, '-') + 1, strlen($id)));
            $this->sheet->getStyle("F13")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    }

    /**
     * @param $from string
     * @param $to string
     */
    public function setPeriod($from, $to)
    {
        $this->sheet->setCellValue("A16", 'Periood:');
        $this->sheet->setCellValue( "B16", date( 'd.m.Y', strtotime(str_replace('/', '.', $from)) ).' - '.date('d.m.Y',strtotime(str_replace('/', '.', $to))) );
    }

    /**
     * @param array $paymentsArray
     * @return int
     */
    public function setMainTable( $paymentsArray = [] )
    {
        $rowNr = 18;

        $mainTable = [];

        //horizontal center align
        $columnsStyle = ['A','C','D'];


        if(isset($paymentsArray['total_sum_online_orders']))
        {
            $mainTable[] = array(
                'A' => '1',
                'B' => 'Tellimused (tasumine online)',
                'C' => 'periood',
                'D' => 1,
                'E' => $paymentsArray['total_sum_online_orders']);
        }

        if(isset($paymentsArray['total_sum_online_couriers']))
        {
            $mainTable[] = array(
                'A' => '2',
                'B' => 'Kulleriteenus (tasumine online)',
                'C' => 'periood',
                'D' => 1,
                'E' => $paymentsArray['total_sum_online_couriers']
            );
        }

        if(isset($paymentsArray['total_sum_online_fee']))
        {
            $mainTable[] = array(
                'A' => '3',
                'B' => 'Vahendustasu (tasumine online)',
                'C' => 'periood',
                'D' => 1,
                'E' => '-'.$paymentsArray['total_sum_online_fee']
            );
        }

        if(isset($paymentsArray['total_sum_cash_fee']))
        {
            $mainTable[] = array(
                'A' => '4',
                'B' => 'Vahendustasu (tasumine toitlustajale)',
                'C' => 'periood',
                'D' => 1,
                'E' => '-'.$paymentsArray['total_sum_cash_fee']
            );
        }


        $rowNr = $this->tableHeader($rowNr, '', true);
        $rowNr++;

        foreach($mainTable as $table)
        {
            foreach($table as $column => $value)
            {
                if(in_array($column, $columnsStyle))
                {
                    $this->sheet->getStyle($column.$rowNr)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }

                $this->sheet->setCellValue($column.$rowNr, $value);
                $this->sheet->getStyle($column.$rowNr)->applyFromArray(self::BORDERS);

            }

            //set formula and borders to last cell in row
            $this->sheet->setCellValue('F'.$rowNr, '=D'.$rowNr.'*E'.$rowNr);

            $this->sheet->getStyle('E'.$rowNr.':F'.$rowNr)
                ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

            $this->sheet->getStyle('F'.$rowNr)->applyFromArray(self::BORDERS);
            $rowNr++;
        }

        $this->sheet->getStyle('A'.$rowNr.':F'.$rowNr)->applyFromArray(self::BORDERS);

        $rowNr++;

        $this->sheet->setCellValue('D'.$rowNr, 'Toitlustajale ülekandmisele kuulub');

        if(isset($paymentsArray['total_sum']))
        {
            $this->sheet->setCellValue('F'.$rowNr, $paymentsArray['total_sum']);
        }

        //styles
        $this->sheet->getStyle('F'.$rowNr)
            ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

        $this->sheet->getStyle('D'.$rowNr.':F'.$rowNr)->applyFromArray(self::BORDERS);
        $this->sheet->getStyle('D'.$rowNr.':F'.$rowNr)->applyFromArray(self::FILL_COLOR);

        $rowNr++;

        $rowNr = $this->drawLine($rowNr);

        return $rowNr;
    }

    /**
     * @param int $rowNr
     * @return int
     */
    public function drawLine( $rowNr = 0 )
    {
        $linesStyle = array(
            'bottom'     => array(
                'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                'color' => array(
                    '	rgb' => '000000'
                )
            )
        );

        $this->sheet->getStyle('A'.$rowNr.':F'.$rowNr)->getBorders()->applyFromArray($linesStyle);

        $rowNr++;

        return $rowNr;
    }

    /**
     * @param int $rowNr
     * @param string $header
     * @param bool|false $mainTable
     * @return int
     * @throws PHPExcel_Exception
     */
    public function tableHeader($rowNr = 0, $header = '', $mainTable = false)
    {
        //styles for all tables header
        $stylesTableHeader = self::BORDERS;
        $stylesTableHeader['alignment'] = array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER
        );


        if($mainTable)
        {
            $tableHead = [
                'A' => 'Nr',
                'B' => 'NIMETUS',
                'C' => 'ÜHIK',
                'D' => 'KOGUS',
                'E' => 'HIND',
                'F' => 'SUMMA, EUR'
            ];
        }else{
            $tableHead = [
                'A' => 'Tellimus',
                'B' => 'Kuupäev',
                'C' => 'Kulleriteenus, eur',
                'D' => 'Toidu väärtus, eur',
                'E' => 'Vahendustasu, %',
                'F' => 'Vahendustasu, eur'
            ];
        }

        if($header)
        {
            $this->sheet->setCellValue('A'.$rowNr, $header);
            $this->sheet->getStyle('A'.$rowNr)->getFont()->setBold(true);

            $rowNr++;
        }


        $this->sheet->getRowDimension($rowNr)->setRowHeight(21);
        foreach($tableHead as $column => $head)
        {
            $this->sheet->setCellValue($column.$rowNr, $head);
            $this->sheet->getStyle($column.$rowNr)->applyFromArray($stylesTableHeader);
        }

        return $rowNr;

    }

    /**
     * @param array $payments
     * @param int $rowNr
     * @param string $header
     * @return int
     */
    public function setPaymentsTable( $payments = [], $rowNr = 0, $header = '' )
    {
        //draw header
        $rowNr = $this->tableHeader($rowNr, $header);

        $rowNr++;

        //array align center to column, set number format 0.00
        $columnsStyles = [
            'A',
            'B',
            array(
                'style' => self::NUMBER_FORMAT,
                'column' => 'C'
            ),
            array(
                'style' => self::NUMBER_FORMAT,
                'column' => 'D'
            ),
            'E',
             array(
                 'style' => self::NUMBER_FORMAT,
                 'column' => 'F'
            ),
        ];

        if(!empty($payments))
        {
            $courier_service = 0;
            $subtotal        = 0;
            $fee_eur         = 0;

            foreach($payments as $payment)
            {

                $courier_service += $payment['courier_service'];
                $subtotal        += $payment['subtotal'];
                $fee_eur         += $payment['fee_eur'];

                //set styles to column
                foreach( $columnsStyles as $column )
                {
                    if( is_array( $column ) )
                    {
                        $this->sheet->getStyle($column['column'].$rowNr)->getNumberFormat()->applyFromArray($column['style']);
                    }else{
                        $this->sheet->getStyle($column.$rowNr)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    }
                }

                $this->sheet->setCellValue('A'.$rowNr, $payment['id']);
                $this->sheet->setCellValue('B'.$rowNr, $payment['date_time']);
                $this->sheet->setCellValue('C'.$rowNr, $payment['courier_service']);
                $this->sheet->setCellValue('D'.$rowNr, $payment['subtotal']);
                $this->sheet->setCellValue('E'.$rowNr, $payment['fee_per']);
                $this->sheet->setCellValue('F'.$rowNr, $payment['fee_eur']);

                $this->sheet->getStyle('A'.$rowNr.':F'.$rowNr)->applyFromArray(self::BORDERS);
                $rowNr++;
            }

            $this->sheet->getStyle('A'.$rowNr.':F'.$rowNr)->applyFromArray(self::BORDERS);
            $rowNr++;

            $this->sheet->setCellValue('B'.$rowNr, 'Kokku (EUR)');
            $this->sheet->setCellValue('C'.$rowNr, $courier_service);
            $this->sheet->setCellValue('D'.$rowNr, $subtotal);
            $this->sheet->setCellValue('F'.$rowNr, $fee_eur);

            //set styles to last row in table
            $this->sheet->getStyle('C'.$rowNr.':F'.$rowNr)
                ->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
            $this->sheet->getStyle('B'.$rowNr)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->sheet->getStyle('B'.$rowNr.':F'.$rowNr)->applyFromArray(self::BORDERS);
            $this->sheet->getStyle('B'.$rowNr.':F'.$rowNr)->applyFromArray(self::FILL_COLOR);

        }
        return $rowNr;
    }

    /**
     * @param int $rowNr
     */
    public function setCompanyDetails( $rowNr = 0 )
    {

        $companyDetails = array(
            'A' => array(
                self::COMPANY_NAME,
                self::ADDRESS,
                self::REG
            ),
            'C' => array(
                self::SITE,
                self::EMAIL,
                self::PHONE
            ),
            'F' => array(
                self::BANK_NAME,
                self::BANK_ACCOUNT,
                self::BANK_SWIFT
            )
        );

        foreach($companyDetails as $column => $details)
        {
            $firstRow = $rowNr;
            foreach($details as $detail)
            {
                $this->sheet->setCellValue($column.$firstRow, $detail);

                if($column == 'F')
                {
                    $this->sheet->getStyle($column.$firstRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                }

                $firstRow++;
            }

        }
    }


    /**
     * @param $imagePath
     * @throws PHPExcel_Exception
     * set logo
     */
    public function setLogo( $imagePath )
    {
        if( $imagePath )
        {
            $this->logo->setPath($imagePath);
            $this->logo->setCoordinates("E1");
            $this->logo->setOffsetX(-25);
            $this->logo->setOffsetY(10);
            $this->logo->setResizeProportional(false);
            $this->logo->setWidth(229);
            $this->logo->setHeight(69);
            $this->logo->setWorksheet($this->sheet);
        }

    }

    public function outputFile()
    {

        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=aruanne.xls" );


        $objWriter = new PHPExcel_Writer_Excel5($this->xls);
        $objWriter->save('php://output');
    }


}