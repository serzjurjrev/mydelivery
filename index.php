<?php

require_once('./createTables.php');

//for logo
$imagePath =  './img/logo.png';

$xls  = new PHPExcel();
$logo = new PHPExcel_Worksheet_Drawing();

$tables = new createTables($xls, $logo);
$tables->createWorkSheetTitle();

//set colums width
$columnsWidthArray = [
    array(
        'column' => 'A',
        'width' => 6.71
    ),
    array(
        'column' => 'B',
        'width' => 30.29
    ),
    array(
        'column' => 'C',
        'width' => 13.57
    ),
    array(
        'column' => 'D',
        'width' => 15.43
    ),
    array(
        'column' => 'E',
        'width' => 13.43
    ),
    array(
        'column' => 'F',
        'width' => 15
    )
];

$tables->setDimenitions( $columnsWidthArray );
$tables->setLogo($imagePath);


$json = '{"total_sum_online_couriers":74.2,"total_sum_online_orders":757.5,"total_sum_online_fee":113.625,"total_sum_cash_couriers":29.9,"total_sum_cash_orders":207,"total_sum_cash_fee":31.05,"total_sum":687.025,"restaurant":"Sushi Guru","restaurant_reg":"Reg.12217124","restaurant_address":"Punane tn 9-49, 13620 Tallinn","restaurant_phone":"37256288444","restaurant_email":"info@tokyo55.ee","report_id":"14 - 09042019","report_created":"09.04.2019","report_period_from":"01\/03\/2019","report_period_to":"31\/03\/2019","report_order_online":[{"id":8908,"date_time":"30.03.2019 21:33","courier_service":"4.90","subtotal":24.5,"fee_per":15,"fee_eur":3.68},{"id":8891,"date_time":"29.03.2019 18:05","courier_service":"3.80","subtotal":41,"fee_per":15,"fee_eur":6.15},{"id":8881,"date_time":"28.03.2019 18:29","courier_service":"3.80","subtotal":24.5,"fee_per":15,"fee_eur":3.68},{"id":8855,"date_time":"23.03.2019 21:49","courier_service":"3.80","subtotal":33,"fee_per":15,"fee_eur":4.95},{"id":8850,"date_time":"23.03.2019 17:42","courier_service":"3.80","subtotal":25,"fee_per":15,"fee_eur":3.75},{"id":8820,"date_time":"20.03.2019 15:02","courier_service":0,"subtotal":64,"fee_per":15,"fee_eur":9.6},{"id":8808,"date_time":"18.03.2019 19:34","courier_service":"3.80","subtotal":22.5,"fee_per":15,"fee_eur":3.38},{"id":8789,"date_time":"17.03.2019 16:44","courier_service":0,"subtotal":51,"fee_per":15,"fee_eur":7.65},{"id":8783,"date_time":"16.03.2019 20:34","courier_service":"3.80","subtotal":23,"fee_per":15,"fee_eur":3.45},{"id":8769,"date_time":"16.03.2019 17:27","courier_service":"3.80","subtotal":22.5,"fee_per":15,"fee_eur":3.38},{"id":8744,"date_time":"15.03.2019 17:21","courier_service":"3.80","subtotal":47,"fee_per":15,"fee_eur":7.05},{"id":8739,"date_time":"15.03.2019 16:41","courier_service":"3.80","subtotal":24,"fee_per":15,"fee_eur":3.6},{"id":8703,"date_time":"09.03.2019 18:24","courier_service":"3.80","subtotal":18,"fee_per":15,"fee_eur":2.7},{"id":8702,"date_time":"09.03.2019 18:20","courier_service":0,"subtotal":51.5,"fee_per":15,"fee_eur":7.73},{"id":8701,"date_time":"09.03.2019 16:58","courier_service":"3.80","subtotal":15.5,"fee_per":15,"fee_eur":2.33},{"id":8697,"date_time":"09.03.2019 12:50","courier_service":"3.80","subtotal":15.5,"fee_per":15,"fee_eur":2.33},{"id":8692,"date_time":"08.03.2019 13:33","courier_service":"3.80","subtotal":32,"fee_per":15,"fee_eur":4.8},{"id":8691,"date_time":"08.03.2019 13:09","courier_service":"3.80","subtotal":32,"fee_per":15,"fee_eur":4.8},{"id":8674,"date_time":"07.03.2019 15:39","courier_service":"3.80","subtotal":41.5,"fee_per":15,"fee_eur":6.23},{"id":8673,"date_time":"07.03.2019 12:42","courier_service":"3.60","subtotal":34,"fee_per":15,"fee_eur":5.1},{"id":8632,"date_time":"02.03.2019 21:50","courier_service":"4.90","subtotal":27.5,"fee_per":15,"fee_eur":4.13},{"id":8627,"date_time":"02.03.2019 18:33","courier_service":0,"subtotal":56,"fee_per":15,"fee_eur":8.4},{"id":8607,"date_time":"01.03.2019 17:11","courier_service":"3.80","subtotal":32,"fee_per":15,"fee_eur":4.8}],"report_order_cash":[{"id":8908,"date_time":"30.03.2019 21:33","courier_service":"4.90","subtotal":24.5,"fee_per":15,"fee_eur":3.68},{"id":8891,"date_time":"29.03.2019 18:05","courier_service":"3.80","subtotal":41,"fee_per":15,"fee_eur":6.15},{"id":8881,"date_time":"28.03.2019 18:29","courier_service":"3.80","subtotal":24.5,"fee_per":15,"fee_eur":3.68},{"id":8855,"date_time":"23.03.2019 21:49","courier_service":"3.80","subtotal":33,"fee_per":15,"fee_eur":4.95},{"id":8850,"date_time":"23.03.2019 17:42","courier_service":"3.80","subtotal":25,"fee_per":15,"fee_eur":3.75},{"id":8820,"date_time":"20.03.2019 15:02","courier_service":0,"subtotal":64,"fee_per":15,"fee_eur":9.6},{"id":8808,"date_time":"18.03.2019 19:34","courier_service":"3.80","subtotal":22.5,"fee_per":15,"fee_eur":3.38},{"id":8789,"date_time":"17.03.2019 16:44","courier_service":0,"subtotal":51,"fee_per":15,"fee_eur":7.65},{"id":8783,"date_time":"16.03.2019 20:34","courier_service":"3.80","subtotal":23,"fee_per":15,"fee_eur":3.45},{"id":8769,"date_time":"16.03.2019 17:27","courier_service":"3.80","subtotal":22.5,"fee_per":15,"fee_eur":3.38},{"id":8744,"date_time":"15.03.2019 17:21","courier_service":"3.80","subtotal":47,"fee_per":15,"fee_eur":7.05},{"id":8739,"date_time":"15.03.2019 16:41","courier_service":"3.80","subtotal":24,"fee_per":15,"fee_eur":3.6},{"id":8703,"date_time":"09.03.2019 18:24","courier_service":"3.80","subtotal":18,"fee_per":15,"fee_eur":2.7},{"id":8702,"date_time":"09.03.2019 18:20","courier_service":0,"subtotal":51.5,"fee_per":15,"fee_eur":7.73},{"id":8701,"date_time":"09.03.2019 16:58","courier_service":"3.80","subtotal":15.5,"fee_per":15,"fee_eur":2.33},{"id":8697,"date_time":"09.03.2019 12:50","courier_service":"3.80","subtotal":15.5,"fee_per":15,"fee_eur":2.33},{"id":8692,"date_time":"08.03.2019 13:33","courier_service":"3.80","subtotal":32,"fee_per":15,"fee_eur":4.8},{"id":8691,"date_time":"08.03.2019 13:09","courier_service":"3.80","subtotal":32,"fee_per":15,"fee_eur":4.8},{"id":8674,"date_time":"07.03.2019 15:39","courier_service":"3.80","subtotal":41.5,"fee_per":15,"fee_eur":6.23},{"id":8673,"date_time":"07.03.2019 12:42","courier_service":"3.60","subtotal":34,"fee_per":15,"fee_eur":5.1},{"id":8632,"date_time":"02.03.2019 21:50","courier_service":"4.90","subtotal":27.5,"fee_per":15,"fee_eur":4.13},{"id":8627,"date_time":"02.03.2019 18:33","courier_service":0,"subtotal":56,"fee_per":15,"fee_eur":8.4},{"id":8607,"date_time":"01.03.2019 17:11","courier_service":"3.80","subtotal":32,"fee_per":15,"fee_eur":4.8}]}';

$tables->drawPaymentsTables(json_decode($json, true));

$tables->outputFile();

